import { Link } from "react-router-dom";
import { Email as EmailIcon, Lock as PasswordIcon } from "@mui/icons-material";
import {
  Button,
  Grid,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import LoginRegister from "../Common/LoginRegister";
import "./login.scss";

type Props = {};

const Login = (props: Props) => {
  return (
    <LoginRegister>
      <Grid item>
        <Typography variant="h4" mb={2}>
          <b> Sign In </b>
        </Typography>
      </Grid>
      <Grid item>
        <TextField
          required
          variant="outlined"
          className="loginFormField"
          placeholder="Email Address"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <EmailIcon />
              </InputAdornment>
            ),
          }}
        />
      </Grid>
      <Grid item>
        <TextField
          required
          variant="outlined"
          className="loginFormField"
          placeholder="Password"
          type="password"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <PasswordIcon />
              </InputAdornment>
            ),
          }}
        />
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          className="loginButton"
          component={Link}
          to="../deploy-firewall"
        >
          {" "}
          Login{" "}
        </Button>
      </Grid>
      <Grid item>
        <Typography
          variant="body2"
          component={Link}
          to="../login"
          className="forgotLink"
        >
          Forgot Password
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant="body2" display="inline">
          Don't have an account?{" "}
        </Typography>
        <Typography
          variant="body2"
          component={Link}
          to="../register"
          className="loginLink"
          display="inline"
        >
          Sign Up
        </Typography>
      </Grid>
    </LoginRegister>
  );
};

export default Login;
