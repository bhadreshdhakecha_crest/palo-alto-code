import React, { useState } from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Container, Box, IconButton } from "@mui/material";
import ModeEditOutlinedIcon from "@mui/icons-material/ModeEditOutlined";
import { useSelector, useDispatch } from "react-redux";
import { useTheme } from "@mui/material/styles";
import awsLogoPng from "../../../assets/images/awsLogoPng.png";
import azureLogoPng from "../../../assets/images/azureLogoPng.png";
import gcpLogo from "../../../assets/images/gcpLogo.png";
import { deployFirewallActions } from "../../../store/store";
import "./Summary.scss";

const Summary = (): JSX.Element => {
  const theme = useTheme();
  const data = useSelector(
    (state: any) => state.deployFirewallReducer.formData
  );
  let image;
  switch (data.cloudProvider) {
    case "aws":
      image = awsLogoPng;
      break;
    case "azure":
      image = azureLogoPng;
      break;
    case "gcp":
      image = gcpLogo;
      break;
  }
  const dispatch = useDispatch();
  const [open, setOpen] = useState({
    acc1: true,
    acc2: true,
    acc3: false,
    acc4: true,
  });

  const doChange = (acc: string) => {
    switch (acc) {
      case "acc1":
        setOpen({ ...open, acc1: !open.acc1 });
        break;
      case "acc2":
        setOpen({ ...open, acc2: !open.acc2 });
        break;
      case "acc3":
        setOpen({ ...open, acc3: !open.acc3 });
        break;
      case "acc4":
        setOpen({ ...open, acc4: !open.acc4 });
        break;
      default:
        setOpen({ ...open });
    }
  };
  return (
    <>
      <Container className="container">
        <Typography variant="h5" className="summary-head">
          Summary
        </Typography>
        <Box>
          <Accordion
            expanded={open.acc1}
            onChange={() => doChange("acc1")}
            className="accordion"
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
              className="summary"
            >
              <Typography>Cloud Provider and Resources</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Box>
                <Box
                  className="box-row"
                  sx={{
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                    },
                  }}
                >
                  <Box className="first-box">
                    <Box className="typography">Cloud Provider Name</Box>
                    <Box className="details">
                      :
                      <img src={image} className="logo-in-summary" alt="Logo" />
                    </Box>
                  </Box>
                  <Box className="first-box">
                    <Box className="typography">Region subnet IP</Box>
                    <Box className="details">: {data.region}</Box>
                    <Box className="icon-button">
                      <IconButton
                        onClick={() =>
                          dispatch(
                            deployFirewallActions.updateFormData({
                              name: "gotToStepOne",
                            })
                          )
                        }
                      >
                        <ModeEditOutlinedIcon />
                      </IconButton>
                    </Box>
                  </Box>
                </Box>

                <Box
                  className="box-row"
                  sx={{
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                    },
                  }}
                >
                  <Box className="first-box">
                    <Box className="typography">AMI ID</Box>
                    <Box className="details">: {data.amiId}</Box>
                  </Box>
                  <Box className="first-box">
                    <Box className="typography">Instance Size</Box>
                    <Box className="details">: {data.instanceSize}</Box>
                    <Box className="icon-button"></Box>
                  </Box>
                </Box>
                <Box
                  className="box-row"
                  sx={{
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                    },
                  }}
                >
                  <Box className="first-box">
                    <Box className="typography">Secret Key</Box>
                    <Box className="details">: {data.secretKey}</Box>
                  </Box>
                  <Box className="first-box">
                    <Box className="typography">Access Key</Box>
                    <Box className="details">: {data.accessKey}</Box>
                    <Box className="icon-button"></Box>
                  </Box>
                </Box>
              </Box>
            </AccordionDetails>
          </Accordion>
          <br />
          <Accordion
            expanded={open.acc2}
            onChange={() => doChange("acc2")}
            className="accordion"
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
              className="summary"
            >
              <Typography>Networking</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Box>
                <Box
                  className="box-row"
                  sx={{
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                    },
                  }}
                >
                  <Box className="first-box">
                    <Box className="typography">VPC Network Name</Box>
                    <Box className="details">: {data.vpcNetworkName}</Box>
                  </Box>
                  <Box className="first-box">
                    <Box className="typography">VPC IP</Box>
                    <Box className="details">: {data.vpcIP}</Box>
                    <Box className="icon-button">
                      <IconButton
                        onClick={() =>
                          dispatch(
                            deployFirewallActions.updateFormData({
                              name: "gotToStepTwo",
                            })
                          )
                        }
                      >
                        <ModeEditOutlinedIcon />
                      </IconButton>
                    </Box>
                  </Box>
                </Box>

                <Box
                  className="box-row"
                  sx={{
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                    },
                  }}
                >
                  <Box className="first-box">
                    <Box className="typography">Web subnet IP</Box>
                    <Box className="details">: {data.webSubnetIP}</Box>
                  </Box>
                  <Box className="first-box">
                    <Box className="typography">Public subnet IP</Box>
                    <Box className="details">: {data.publicSubnetIP}</Box>
                    <Box className="icon-button"></Box>
                  </Box>
                </Box>
              </Box>
            </AccordionDetails>
          </Accordion>
          <br />
          <Accordion
            expanded={open.acc4}
            onChange={() => doChange("acc4")}
            className="accordion"
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel4a-content"
              id="panel4a-header"
              className="summary"
            >
              <Typography>Management interface</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Box>
                <Box
                  className="box-row"
                  sx={{
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                    },
                  }}
                >
                  <Box className="first-box">
                    <Box className="typography">Bucket Name</Box>
                    <Box className="details">: {data.bucketName}</Box>
                  </Box>
                  <Box className="first-box">
                    <Box className="typography">Stack Name</Box>
                    <Box className="details">: {data.stackName}</Box>
                    <Box className="icon-button">
                      <IconButton
                        onClick={() =>
                          dispatch(
                            deployFirewallActions.updateFormData({
                              name: "gotToStepThree",
                            })
                          )
                        }
                      >
                        <ModeEditOutlinedIcon />
                      </IconButton>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </AccordionDetails>
          </Accordion>
        </Box>
      </Container>
    </>
  );
};

export default Summary;
