import * as React from "react";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import Button from "@mui/material/Button";
import SideBar from "../../Common/SideBar";
import {
  Container,
  FormControl,
  Grid,
  MenuItem,
  TextField,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import awsLogo from "../../../assets/images/awsLogoPng.png";
import azureLogo from "../../../assets/images/azureLogoPng.png";
import gcpLogo from "../../../assets/images/gcpLogo.png";
import { useSelector, useDispatch } from "react-redux";
import { deployFirewallActions } from "../../../store/store";
import Summary from "../Summary/Summary";
import "./Deploy.scss";
import FirewallList from "../FirewallList/FirewallList";

const steps = [
  "Cloud Provider",
  "Resources",
  "Networking",
  "Management interface",
];

const Deploy = () => {
  const theme = useTheme();
  const [skipped, setSkipped] = React.useState(new Set<number>());
  const data = useSelector(
    (state: any) => state.deployFirewallReducer.formData
  );
  const dispatch = useDispatch();
  const arrRegionsAndAmiId = [
    {
      region: "us-west-2 (North America)",
      amiId: "ami-d424b5ac",
    },
    {
      region: "us-northeast-1 (North America)",
      amiId: "ami-00d61562",
    },
    {
      region: "ca-central-1 (China)",
      amiId: "ami-3780d988",
    },
    {
      region: "sa-east-1 (South Asia)",
      amiId: "ami-55bfd73a",
    },
    {
      region: "eu-south-2 (Europe)",
      amiId: "ami-9c0154f0",
    },
  ];
  const arrInstanceSize = [
    "c3.2xlarge",
    "c4.xlarge",
    "c4.2xlarge",
    "c4.8xlarge",
    "c5.4xlarge",
  ];
  const arrStep0 = [
    {
      value: awsLogo,
      field: "aws",
    },
    {
      value: gcpLogo,
      field: "gcp",
    },
    {
      value: azureLogo,
      field: "azure",
    },
  ];
  const arrStep1 = [
    {
      name: "region",
      typography: "Region",
      helper: "Choose specific region",
      validRegEx: / /,
      changed: true,
      value: data.region,
    },
    {
      name: "AmiId",
      typography: "AMI ID",
      helper: "AMI ID is for the vm series firewall",
      validRegEx: / /,
      changed: true,
      value: data.amiId,
    },
    {
      name: "instanceSize",
      typography: "Instance Size",
      helper: "Size of the VM instance",
      validRegEx: / /,
      changed: true,
      value: data.instanceSize,
    },
    {
      name: "accessKey",
      typography: "Access Key",
      helper: "Enter Access keys to make programmatic calls to AWS",
      validRegEx: /(?<![A-Z0-9])[A-Z0-9]{20}(?![A-Z0-9])/,
      changed: false,
      value: data.accessKey,
    },
    {
      name: "secretKey",
      typography: "Secret Key",
      helper: "Enter the AWS Secret Key (must have 40 chars)",
      validRegEx: /(?<![A-Za-z0-9/+=])[A-Za-z0-9/+=]{40}(?![A-Za-z0-9/+=])/,
      changed: false,
      value: data.secretKey,
    },
  ];
  const arrStep2 = [
    {
      name: "vpcName",
      typography: "VPC Network Name",
      helper: "Creates a tag with a key of 'Name' and a value that you specify",
      validRegEx:
        /^vpc-(ue1|uw1|uw2|ew1|ec1|an1|an2|as1|as2|se1)-(d|t|s|p)-([a-z0-9-]+)$/,
      changed: false,
      value: data.vpcNetworkName,
    },
    {
      name: "vpcIP",
      typography: "VPC IP",
      helper: "VPC IP range",
      validRegEx: /^([0-9]{1,3}\.){3}[0-9]{1,3}\/(1[6-9]|2[0-8])$/,
      changed: false,
      value: data.vpcIP,
    },
    {
      name: "webSubIP",
      typography: "Web Subnet IP",
      helper: "Web Subnet IP Range",
      validRegEx: /^([0-9]{1,3}\.){3}[0-9]{1,3}\/(1[6-9]|2[0-8])$/,
      changed: false,
      value: data.webSubnetIP,
    },
    {
      name: "pubSubIP",
      typography: "Public Subnet IP",
      helper: "Public Subnet IP Range",
      validRegEx: /^([0-9]{1,3}\.){3}[0-9]{1,3}\/(1[6-9]|2[0-8])$/,
      changed: false,
      value: data.publicSubnetIP,
    },
  ];
  const arrStep3 = [
    {
      name: "serverKey",
      typography: "Server Key",
      helper: "SSH Key Pair Name stored in AWS",
      validRegEx: / /,
      changed: false,
      value: data.serverKey,
    },
    {
      name: "stackName",
      typography: "Stack Name",
      helper: "Unique identifier tag",
      validRegEx: /^([\p{L}\p{Z}\p{N}_.:/=+\-@]*)$/,
      changed: false,
      value: data.stackName,
    },
    {
      name: "bucketName",
      typography: "Bucket Name",
      helper: "AWS S3 Bucket Name",
      validRegEx: /^((?!xn--)(?!.*-s3alias)[a-z0-9][a-z0-9.-]{1,62}[a-z0-9])$/,
      changed: false,
      value: data.bucketName,
    },
  ];
  const handleNext = () => {
    let newSkipped = skipped;
    setSkipped(newSkipped);
    dispatch(deployFirewallActions.updateFormData({ name: "nextStep" }));
  };

  const handleBack = () => {
    if (data.activeStep === 0)
      dispatch(deployFirewallActions.updateFormData({ name: "noFirewall" }));
    dispatch(deployFirewallActions.updateFormData({ name: "backStep" }));
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const name = event.target.name;
    let ami;
    for (let x of arrRegionsAndAmiId) {
      if (x.region === event.target.value) ami = x.amiId;
    }

    if (name === "region") {
      dispatch(
        deployFirewallActions.updateFormData({
          name: name,
          value: event.target.value,
          amiId: ami,
        })
      );
    } else {
      dispatch(
        deployFirewallActions.updateFormData({
          name: name,
          value: event.target.value,
        })
      );
    }
  };
  let flag: boolean = false;
  const [loaded, setLoaded] = React.useState(0);

  const checkValidOrNot = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    if (
      data.activeStep === 2 &&
      data.vpcNetworkName === "" &&
      data.vpcIP === "" &&
      data.webSubnetIP === "" &&
      data.publicSubnetIP === ""
    ) {
      setLoaded(1);
    }
    if (
      data.activeStep === 1 &&
      data.instanceSize === "" &&
      data.secretKey === "" &&
      data.accessKey === ""
    ) {
      setLoaded(1);
    }
    if (
      data.activeStep === 3 &&
      data.serverKey === "" &&
      data.bucketName === "" &&
      data.stackName === ""
    ) {
      setLoaded(1);
    }
  };

  const fillForm = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    flag = false;
    if (
      data.activeStep === 1 &&
      data.clodProvider !== "" &&
      data.instanceSize !== "" &&
      arrStep1[3].validRegEx.test(data.accessKey) &&
      arrStep1[4].validRegEx.test(data.secretKey)
    ) {
      flag = true;
      setLoaded(0);
    }
    if (
      data.activeStep === 2 &&
      arrStep2[0].validRegEx.test(data.vpcNetworkName) &&
      arrStep2[1].validRegEx.test(data.vpcIP) &&
      arrStep2[2].validRegEx.test(data.webSubnetIP) &&
      arrStep2[3].validRegEx.test(data.publicSubnetIP)
    ) {
      flag = true;
      setLoaded(0);
    }
    if (
      data.activeStep === 3 &&
      arrStep3[0].validRegEx.test(data.serverKey) &&
      arrStep3[1].validRegEx.test(data.stackName) &&
      arrStep3[2].validRegEx.test(data.bucketName)
    ) {
      flag = true;
      setLoaded(0);
    }
    console.log(loaded);
    console.log(flag);
    if (flag) {
      dispatch(deployFirewallActions.updateFormData({ name: "nextStep" }));
    }
  };
  return (
    <SideBar>
      {!data.noFirewall && <FirewallList/>}
      <Container>
        <Box
          sx={{
            width: "100%",
            [theme.breakpoints.down("sm")]: {
              width: "360px",
            },
            [theme.breakpoints.down("xs")]: {
              width: "200px",
            },
          }}
        >
          {data.activeStep !== 4 && data.activeStep !== -1 && data.noFirewall && (
            <Box className="stepper">
              <Stepper
                className="inside"
                activeStep={data.activeStep}
                alternativeLabel
              >
                {steps.map((label, index) => {
                  const stepProps: { completed?: boolean } = {};
                  const labelProps: {
                    optional?: React.ReactNode;
                  } = {};
                  return (
                    <Step key={label} {...stepProps}>
                      <StepLabel {...labelProps}>{label}</StepLabel>
                    </Step>
                  );
                })}
              </Stepper>
            </Box>
          )}
          {data.noFirewall && (
            <Box
              className="step-box"
              sx={{ ...(data.activeStep === 1 && { minHeight: "520px" }) }}
            >
              <Box
                sx={{
                  maxWidth: 600,
                  margin: "auto",
                  paddingTop: "50px",
                  marginTop: "30px",
                }}
              >
                {data.activeStep === -1 && data.noFirewall && (
                  <Box className="no-firewall">
                    <InfoOutlinedIcon color="disabled" fontSize="large" />
                    <Typography className="heading5" variant="h5">
                      No Firewalls Found
                    </Typography>
                    {data.activeStep === -1 && (
                      <Button
                        onClick={handleNext}
                        variant="contained"
                        className="btn2"
                      >
                        Deploy Firewall
                      </Button>
                    )}
                  </Box>
                )}

                <FormControl fullWidth>
                  {data.activeStep === 0 && (
                    <>
                      <Typography
                        sx={{ paddingBottom: 4, textAlign: "center" }}
                        variant="body1"
                      >
                        Select Cloud Service Provider
                      </Typography>
                      <Grid container spacing={2}>
                        {arrStep0.map((cloudProvider, index) => {
                          return (
                            <Grid item xs={4}>
                              <Box
                                onClick={() =>
                                  dispatch(
                                    deployFirewallActions.updateFormData({
                                      name: "cloudProvider",
                                      value: cloudProvider.field,
                                    })
                                  )
                                }
                                key={"cloudLogo " + index}
                                className="paper"
                                sx={{
                                  ...(data.cloudProvider ===
                                    cloudProvider.field && {
                                    backgroundColor: "#ECF1F6",
                                  }),
                                }}
                              >
                                <img
                                  className="cloud-provider-logo"
                                  src={cloudProvider.value}
                                  alt={cloudProvider.field + "Logo"}
                                />
                              </Box>
                            </Grid>
                          );
                        })}
                      </Grid>
                    </>
                  )}

                  {data.activeStep === 1 && (
                    <form id="step-form" onSubmit={fillForm}>
                      {arrStep1.map((field, index) => {
                        return (
                          <Box className="input-box">
                            <Typography
                              sx={{
                                paddingRight: 3,
                                marginTop: -3,
                              }}
                              className="input-label"
                            >
                              {field.typography}
                            </Typography>
                            <TextField
                              id={"arrStep1 " + index}
                              name={field.name}
                              select={(index === 0 || index === 2) && true}
                              value={field.value}
                              onChange={handleChange}

                              helperText={
                                index > 2 && loaded === 1
                                  ? field.validRegEx.test(field.value)
                                    ? field.helper
                                    : "Please enter Valid Input"
                                  : field.helper
                              }
                              className="input-text-field"
                              size="small"
                              error={
                                index > 2 &&
                                ((field.value !== "" &&
                                !field.validRegEx.test(field.value)
                                  ? true
                                  : false) ||
                                  (loaded === 1 && field.value === ""
                                    ? true
                                    : false))
                              }
                              required
                              disabled={index === 1 && true}
                            >
                              {index <= 1 &&
                                arrRegionsAndAmiId.map((reg) => (
                                  <MenuItem
                                    key={index === 0 ? reg.region : reg.amiId}
                                    value={index === 0 ? reg.region : reg.amiId}
                                  >
                                    {index === 0 ? reg.region : reg.amiId}
                                  </MenuItem>
                                ))}
                              {index === 2 &&
                                arrInstanceSize.map((ins) => (
                                  <MenuItem key={ins} value={ins}>
                                    {ins}
                                  </MenuItem>
                                ))}
                            </TextField>
                          </Box>
                        );
                      })}
                    </form>
                  )}

                  {data.activeStep === 2 && (
                    <form id="step-form" onSubmit={fillForm}>
                      {arrStep2.map((field, index) => {
                        return (
                          <Box className="input-box">
                            <Typography
                              sx={{
                                paddingRight: 3,
                                marginTop: -3,
                              }}
                              className="input-label"
                            >
                              {field.typography}
                            </Typography>
                            <TextField
                              id={"arrStep2 " + index}
                              name={field.name}
                              value={field.value}
                              onChange={handleChange}
                              helperText={
                                loaded === 1
                                  ? field.validRegEx.test(field.value)
                                    ? field.helper
                                    : "Please enter Valid Input"
                                  : field.helper
                              }
                              className="input-text-field"
                              size="small"
                              required
                              error={
                                (field.value !== "" &&
                                !field.validRegEx.test(field.value)
                                  ? true
                                  : false) ||
                                (loaded === 1 && field.value === ""
                                  ? true
                                  : false)
                              }
                            ></TextField>
                          </Box>
                        );
                      })}
                    </form>
                  )}

                  {data.activeStep === 3 && (
                    <form id="step-form" onSubmit={fillForm}>
                      {arrStep3.map((field, index) => {
                        return (
                          <Box className="input-box">
                            <Typography
                              sx={{
                                paddingRight: 3,
                                marginTop: -3,
                              }}
                              className="input-label"
                            >
                              {field.typography}
                            </Typography>
                            <TextField
                              id={"arrStep3 " + index}
                              name={field.name}
                              value={field.value}
                              onChange={handleChange}
                              helperText={
                                loaded === 1
                                  ? field.validRegEx.test(field.value)
                                    ? field.helper
                                    : "Please enter Valid Input"
                                  : field.helper
                              }
                              className="input-text-field"
                              size="small"
                              error={
                                index > 0 &&
                                ((field.value !== "" &&
                                !field.validRegEx.test(field.value)
                                  ? true
                                  : false) ||
                                  (loaded === 1 && field.value === ""
                                    ? true
                                    : false))
                              }
                              required
                            ></TextField>
                          </Box>
                        );
                      })}
                    </form>
                  )}
                </FormControl>
              </Box>
              {data.activeStep === 4 && (
                <>
                  <Summary />
                </>
              )}
              <Box className="btn-grp">
                {data.activeStep > 0 && (
                  <Button
                    color="inherit"
                    disabled={data.activeStep === 0}
                    onClick={handleBack}
                    sx={{ mr: 1 }}
                    variant="contained"
                    className="btn back-btn"
                  >
                    Back
                  </Button>
                )}
                {data.activeStep >= 1 && (
                  <>
                    {data.activeStep !== steps.length && (
                      <Button
                        // onClick={handleNext}
                        className="btn"
                        variant="contained"
                        type="submit"
                        form="step-form"
                        onClick={checkValidOrNot}
                      >
                        {data.activeStep < steps.length - 1 &&
                          data.activeStep > 0 &&
                          "Next"}
                        {data.activeStep === steps.length - 1 && "Finish"}
                      </Button>
                    )}
                    {data.activeStep === steps.length && (
                      <Button className="btn" variant="contained">
                        Deploy
                      </Button>
                    )}
                  </>
                )}
                {data.activeStep === 0 && (
                  <Button
                    onClick={handleBack}
                    className="btn"
                    variant="contained"
                  >
                    Cancel
                  </Button>
                )}
              </Box>
            </Box>
          )}
        </Box>
      </Container>
    </SideBar>
  );
};

export default Deploy;
