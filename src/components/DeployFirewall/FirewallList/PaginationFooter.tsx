import { Dispatch, SetStateAction } from "react";
import { MenuItem, Pagination, Select, Stack, Typography } from "@mui/material";
import { rowsPage } from "../../Common/dataString";

type Props = {
  rowsPerPage: number;
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  count: number;
  setRowsPerPage: Dispatch<SetStateAction<number>>;
};

const PaginationFooter = (props: Props) => {
  const { rowsPerPage, page, setPage, count, setRowsPerPage } = props;
  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };
  return (
    <Stack
      direction="row"
      spacing={2}
      justifyContent="flex-end"
      alignItems="center"
      p={1}
    >
      <Typography variant="body2">{rowsPage}</Typography>
      <Select
        value={rowsPerPage}
        size="small"
        onChange={(event) => {
          if (typeof event.target.value === "string")
            setRowsPerPage(parseInt(event.target.value, 10));
          else setRowsPerPage(event.target.value);
          setPage(1);
        }}
      >
        <MenuItem value={5}>5</MenuItem>
        <MenuItem value={10}>10</MenuItem>
        <MenuItem value={25}>25</MenuItem>
      </Select>
      <Pagination
        className="table-page"
        count={count}
        page={page}
        variant="outlined"
        shape="rounded"
        onChange={handleChangePage}
      />
    </Stack>
  );
};

export default PaginationFooter;
