export const columns: string[] = [
    'deployment_id',
    'firewall_name',
    'cloud_provider',
    'status',
    'public_ip',
    'created_on',
    'logs',
];

export interface Data {
    deployment_id: number;
    firewall_name: string;
    cloud_provider: string;
    status: string;
    public_ip: string;
    region: string;
    created_on: string;
    log_number: string;
}

interface HeadCell {
    disablePadding: boolean;
    id: keyof Data;
    label: string;
    postionCenter: boolean;
    width: string;
}

export const headCells: readonly HeadCell[] = [
    {
        id: 'deployment_id',
        postionCenter: true,
        disablePadding: true,
        label: 'Deployment Id',
        width: '12%',
    },
    {
        id: 'firewall_name',
        postionCenter: true,
        disablePadding: true,
        label: 'Firewall Name',
        width: '12%',
    },
    {
        id: 'cloud_provider',
        postionCenter: true,
        disablePadding: true,
        label: 'Cloud Provider',
        width: '14%',
    },
    {
        id: 'status',
        postionCenter: true,
        disablePadding: true,
        label: 'Status',
        width: '14%',
    },
    {
        id: 'public_ip',
        postionCenter: true,
        disablePadding: true,
        label: 'Public IP',
        width: '14%',
    },
    {
        id: 'created_on',
        postionCenter: true,
        disablePadding: true,
        label: 'Creation Date',
        width: '18%',
    },
    {
        id: 'log_number',
        postionCenter: true,
        disablePadding: false,
        label: 'Logs',
        width: '10%',
    },
];

// function createData(
//     id: number,
//     firewall: string,
//     cloud: string,
//     status: string,
//     ip: string,
//     creation: Date,
//     logs: string
// ): Data {
//     return {
//         id,
//         firewall,
//         cloud,
//         status,
//         ip,
//         creation,
//         logs,
//     };
// }

// export const rows = [
//     createData(
//         1,
//         'next-generation firewall',
//         'aws',
//         'Deployed',
//         '172.35.68.79',
//         new Date('2022-03-18T11:11:54'),
//         'log-1'
//     ),
//     createData(
//         2,
//         'stateful inspection firewall',
//         'gcp',
//         'Deployed',
//         '172.77.67.44',
//         new Date('2022-02-23T21:15:17'),
//         'log-2'
//     ),
//     createData(
//         3,
//         'application-level gateway',
//         'azure',
//         'Deployed',
//         '172.21.6.9',
//         new Date('2022-03-25T08:08:56'),
//         'log-3'
//     ),
//     createData(
//         4,
//         'circuit-level gateway',
//         'gcp',
//         'Failed',
//         '172.23.34.49',
//         new Date('2022-03-28T04:15:34'),
//         'log-4'
//     ),
//     createData(
//         5,
//         'packet filtering firewall',
//         'aws',
//         'In Progress',
//         '172.25.3.7',
//         new Date('2022-03-30T22:41:54'),
//         'log-5'
//     ),
// ];
