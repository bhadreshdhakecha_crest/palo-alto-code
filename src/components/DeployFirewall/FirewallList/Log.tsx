import * as React from 'react';
import clsx from 'clsx';
import { withStyles, WithStyles } from '@mui/styles';
import { Theme, createTheme } from '@mui/material/styles';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import {
  AutoSizer,
  Column,
  Table,
  TableCellRenderer,
  TableHeaderProps,
} from 'react-virtualized';

const MuiTableCell = withStyles({
  root: {
    borderBottom: "none"
  }
})(TableCell);

const styles = (theme: Theme) =>
  ({
    flexContainer: {
      display: 'flex',
      alignItems: 'center',
      boxSizing: 'border-box',
    },
    table: {
      // temporary right-to-left patch, waiting for
      // https://github.com/bvaughn/react-virtualized/issues/454
      '& .ReactVirtualized__Table__headerRow': {
        ...(theme.direction === 'rtl' && {
          paddingLeft: '0 !important',
        }),
        ...(theme.direction !== 'rtl' && {
          paddingRight: undefined,
        }),
      },
    },
    tableRow: {
      cursor: 'pointer',
    },
    tableRowHover: {
      '&:hover': {
        backgroundColor: theme.palette.grey[200],
      },
    },
    tableCell: {
      flex: 1,
    },
    noClick: {
      cursor: 'initial',
    },
  } as const);

interface ColumnData {
  dataKey: string;
  label: string;
  numeric?: boolean;
  width: number;
}

interface Row {
  index: number;
}

interface MuiVirtualizedTableProps extends WithStyles<typeof styles> {
  columns: readonly ColumnData[];
  headerHeight?: number;
  onRowClick?: () => void;
  rowCount: number;
  rowGetter: (row: Row) => Data;
  rowHeight?: number;
}

class MuiVirtualizedTable extends React.PureComponent<MuiVirtualizedTableProps> {
  static defaultProps = {
    headerHeight: 48,
    rowHeight: 35,
  };

  getRowClassName = ({ index }: Row) => {
    const { classes, onRowClick } = this.props;

    return clsx(classes.tableRow, classes.flexContainer, {
      [classes.tableRowHover]: index !== -1 && onRowClick != null,
    });
  };

  cellRenderer: TableCellRenderer = ({ cellData, columnIndex }) => {
    const { columns, classes, rowHeight, onRowClick } = this.props;
    
    return (
      <MuiTableCell
        component="div"
        className={clsx(classes.tableCell, classes.flexContainer, {
          [classes.noClick]: onRowClick == null,
        })}
        variant="body"
        style={{ height: rowHeight }}
        align={
          (columnIndex != null && columns[columnIndex].numeric) || false
            ? 'right'
            : 'left'
        }
        sx={{
          whiteSpace: "normal",
          wordBreak: "break-all",
        }}
      >
        <span className="log-data">{cellData}</span>
      </MuiTableCell>
    );
  };

  headerRenderer = ({
    label,
    columnIndex,
  }: TableHeaderProps & { columnIndex: number }) => {
    const { headerHeight, columns, classes } = this.props;

    return (
      <TableCell
        component="div"
        className={clsx(classes.tableCell, classes.flexContainer, classes.noClick)}
        variant="head"
        style={{ height: headerHeight }}
        align={columns[columnIndex].numeric || false ? 'right' : 'left'}
      >
        <span className='log-header'>{label}</span>
      </TableCell>
    );
  };

  render() {
    const { classes, columns, rowHeight, headerHeight, ...tableProps } = this.props;
    return (
      <AutoSizer>
        {({ height, width }) => (
          <Table
            height={height}
            width={width}
            rowHeight={rowHeight!}
            gridStyle={{
              direction: 'inherit',
            }}
            headerHeight={headerHeight!}
            className={classes.table}
            {...tableProps}
            rowClassName={this.getRowClassName}
          >
            {columns.map(({ dataKey, ...other }, index) => {
              return (
                <Column
                  key={dataKey}
                  headerRenderer={(headerProps) =>
                    this.headerRenderer({
                      ...headerProps,
                      columnIndex: index,
                    })
                  }
                  className={classes.flexContainer}
                  cellRenderer={this.cellRenderer}
                  dataKey={dataKey}
                  {...other}
                />
              );
            })}
          </Table>
        )}
      </AutoSizer>
    );
  }
}

const defaultTheme = createTheme();
const VirtualizedTable = withStyles(styles, { defaultTheme })(MuiVirtualizedTable);

// ---

interface Data {
  time: string;
  event: string;
  description: string;
}

function createData(
  time: string,
  event: string,
  description: string
): Data {
  return { time, event, description };
}

const rows: Data[] = [createData("24-01-2022 11:31:26","TRACE","===============[ Logger initialized ]==============="),
createData("24-01-2022 11:31:26","ENTER","> Entering wmain"),
createData("24-01-2022 11:31:26","TRACE","argc=2"),
createData("24-01-2022 11:31:26","INFO","DLL Filename=C:\\Users\\krishna.domadiya\\AppData\\Local\\Temp\\ConfigUpdater.dll"),
createData("24-01-2022 11:31:26","INFO","Library loaded successfully"),
createData("24-01-2022 11:31:26","TRACE","Calling Execute()..."),
createData("24-01-2022 11:31:33","INFO","Execute() returned, res=1"),
createData("24-01-2022 11:31:33","LEAVE","< Leaving wmain"),
createData("24-01-2022 11:31:33","TRACE","===============[   Logger cleanup   ]==============="),
createData("24-01-2022 11:33:14","TRACE","===============[ Logger initialized ]==============="),
createData("24-01-2022 11:33:14","ENTER","> Entering wmain"),
createData("24-01-2022 11:33:14","TRACE","argc=2"),
createData("24-01-2022 11:33:14","INFO","DLL Filename=C:\\Users\\krishna.domadiya\\AppData\\Local\\Temp\\ConfigUpdater.dll"),
createData("24-01-2022 11:33:14","INFO","Library loaded successfully"),
createData("24-01-2022 11:33:14","TRACE","Calling Execute()..."),
createData("24-01-2022 11:33:14","INFO","Execute() returned, res=1"),
createData("24-01-2022 11:33:14","TRACE","===============[   Logger cleanup   ]===============")];

export default function Log() {
  return (
    <Paper style={{ height: 400, width: '100%' }}>
      <VirtualizedTable
        rowCount={rows.length}
        rowGetter={({ index }) => rows[index]}
        columns={[
          {
            width: 165,
            label: 'Time',
            dataKey: 'time',
          },
          {
            width: 80,
            label: 'Event',
            dataKey: 'event',
          },
          {
            width: 435,
            label: 'Description',
            dataKey: 'description',
          },
        ]}
      />
    </Paper>
  );
}
