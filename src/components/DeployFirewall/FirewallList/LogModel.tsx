import { Dispatch, SetStateAction } from "react";
import { Box, Button, Divider, Grid, IconButton, Modal, Stack, Typography } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { logModelTitle } from "../../Common/dataString";
import Log from "./Log";

type Props = {
    openLog : boolean;
    setOpenLog : Dispatch<SetStateAction<boolean>>;
};

const LogModel = (props: Props) => {
    const {openLog, setOpenLog} = props;
  return (
    <Modal
      open={openLog}
      onClose={() => setOpenLog(false)}
      aria-labelledby="log-modal"
      aria-describedby="display-log"
    >
      <Box className="log-modal">
        <Box className="log-modal-header">
          <Grid container>
            <Grid item xs={10}>
              <Typography m={1} variant="body1">
                <b>{logModelTitle}</b>
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Stack direction="row" justifyContent="end">
                <IconButton
                  aria-label="close"
                  size="large"
                  onClick={() => setOpenLog(false)}
                >
                  <CloseIcon />
                </IconButton>
              </Stack>
            </Grid>
          </Grid>
          <Divider />
        </Box>
        <Box className="log-modal-content">
          <Log />
        </Box>
        <Box className="log-modal-footer">
          <Divider />
          <Stack direction="row" justifyContent="end" mt={1} mr={2}>
            <Button
              variant="contained"
              size="small"
              className="close-modal-button"
              onClick={() => setOpenLog(false)}
            >
              Close
            </Button>
          </Stack>
        </Box>
      </Box>
    </Modal>
  );
};

export default LogModel;
