import { Dispatch, SetStateAction } from "react";
import {
  Button,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  OutlinedInput,
  Stack,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import RefreshIcon from "@mui/icons-material/Refresh";
import { buttonDeployNew } from "../../Common/dataString";
import { useDispatch } from "react-redux";
import { deployFirewallActions } from "../../../store/store";

type Props = {
  searched: String;
  setSearched: Dispatch<SetStateAction<String>>;
  refresh: () => void;
  setPage: Dispatch<SetStateAction<number>>;
};

const Head = (props: Props) => {
  const { searched, setSearched, refresh, setPage } = props;
  const dispatch=useDispatch();
  //for next step
  const goToNextStep=(event: React.MouseEvent<HTMLButtonElement, MouseEvent>)=>{
    dispatch(deployFirewallActions.updateFormData({name:"nextStep"}));
    dispatch(deployFirewallActions.updateFormData({name:"noFirewall"}));
  }
  return (
    <Grid container>
      <Grid item xs>
        <FormControl sx={{ mb: 2, width: "35ch" }} variant="outlined">
          <OutlinedInput
            value={searched}
            size="small"
            placeholder="Search"
            onChange={(event) => {setSearched(event.target.value); setPage(1);}}
            startAdornment={
              <InputAdornment position="start">
                <IconButton edge="start">
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            }
            type="search"
            inputProps={{
              "aria-label": "weight",
            }}
          />
        </FormControl>
      </Grid>
      <Grid item xs>
        <Stack direction="row" justifyContent="end" spacing={2}>
          <IconButton className="style-refresh-button" onClick={refresh}>
            <RefreshIcon />
          </IconButton>
          <Button variant="contained" onClick={goToNextStep} className="style-button">
            {buttonDeployNew}
          </Button>
        </Stack>
      </Grid>
    </Grid>
  );
};

export default Head;
