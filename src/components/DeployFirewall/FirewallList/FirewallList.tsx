import React, { useEffect } from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import { Description, ExpandLess, ExpandMore, Launch } from '@mui/icons-material';
import { Data } from './data';
import { Chip, Collapse, Fade, Grid, LinearProgress, Tooltip, Typography } from '@mui/material';
import axios from 'axios';
import './FirewallList.scss';
import gcp from '../../../assets/images/gcp.png';
import azure from '../../../assets/images/azure.png';
import aws from '../../../assets/images/aws.png';
import CopyIcon from '../../Common/CopyIcon';
import { getComparator, Order, stableSort } from '../../Common/Table/funcTable';
import EnhancedTableHead from '../../Common/Table/EnhancedTableHead';
import { errorFound, firewallInitialized, noRowFound } from '../../Common/dataString';
import AlertBox from '../../Common/AlertBox';
import Head from './Head';
import PaginationFooter from './PaginationFooter';
import LogModel from './LogModel';

const FirewallList = () => {
    const [order, setOrder] = React.useState<Order>('desc');
    const [orderBy, setOrderBy] = React.useState<keyof Data>('created_on');
    const [selected, setSelected] = React.useState<readonly number[]>([]);
    const [page, setPage] = React.useState(1);
    const [openAlert, setOpenAlert] = React.useState<boolean>(true);
    let emptyTable = false;
    const [isError, setIsError] = React.useState<boolean>(false);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    // const classes = useRowStyles();
    const [searched, setSearched] = React.useState<String>('');
    const [displayLaunchIcon, setDisplayLaunchIcon] = React.useState<number>(-1);
    const [openLog, setOpenLog] = React.useState<boolean>(false);
    const [rows, setRows] = React.useState<Data[]>([]);
    const [openCopyChip, setOpenCopyChip] = React.useState<boolean>(false);
    const [version, setVersion] = React.useState<number>(0);
    const [isLoading, setIsLoading] = React.useState<boolean>(false);
    // when called, add 1 to "version"
    const refresh = React.useCallback(() => {
        setVersion((s) => s + 1);
    }, []);

    useEffect(() => {
        const options = {
            params: {
                page_size: rowsPerPage,
                page_number: page,
                query: searched,
            },
        };
        setIsError(false);
        setIsLoading(true);
        axios
            .get('/api/firewalls', options)
            .then((res) => {
                // setGroup(res.data)
                setRows(res.data.firewalls);
                console.log(res.data.firewalls.length);
            })
            .catch((error) => {
                setIsError(true);
                console.log(error);
            })
            .finally(() => {
                setIsLoading(false);
            });
    }, [page, rowsPerPage, version, searched]);

    const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof Data) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleClick = (event: React.MouseEvent<unknown>, id: number) => {
        const selectedIndex = selected.indexOf(id);
        let newSelected: readonly number[] = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
        }

        setSelected(newSelected);
    };

    const isSelected = (id: number) => selected.indexOf(id) !== -1;

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows = page > 1 ? Math.max(0, page * rowsPerPage - 15) : 0;

    return (
        <>
            <Box mx={1}>
                <AlertBox open={openAlert} setOpen={setOpenAlert} value={firewallInitialized} />
                <Head searched={searched} setSearched={setSearched} refresh={refresh} setPage={setPage} />
                <Paper sx={{ width: '100%', mb: 2 }} elevation={2}>
                    <TableContainer>
                        <Table sx={{ minWidth: 900, tableLayout: 'fixed' }} aria-labelledby='tableTitle' size='small'>
                            <EnhancedTableHead order={order} orderBy={orderBy} onRequestSort={handleRequestSort} />
                            {!isLoading && !isError && (
                                <TableBody>
                                    {/* if you don't need to support IE11, you can replace the `stableSort` call with:
              rows.slice().sort(getComparator(order, orderBy)) */}
                                    {stableSort(rows, getComparator(order, orderBy))
                                        // .slice(
                                        //   (page - 1) * rowsPerPage,
                                        //   (page - 1) * rowsPerPage + rowsPerPage
                                        // )
                                        // .filter(
                                        //     (row) =>
                                        //         row.cloud_provider.toLowerCase().includes(searched?.toLowerCase()) ||
                                        //         row.cloud_provider.toLowerCase().includes(searched?.toLowerCase()) ||
                                        //         row.deployment_id.toString().includes(searched.toString()) ||
                                        //         row.status.toLowerCase().includes(searched?.toLowerCase()) ||
                                        //         row.public_ip.toLowerCase().includes(searched?.toLowerCase()) ||
                                        //         row.created_on.toLowerCase().includes(searched.toLowerCase())
                                        // )
                                        .map((row, index) => {
                                            const isItemSelected = isSelected(row.deployment_id);
                                            const labelId = `enhanced-table-checkbox-${index}`;
                                            emptyTable = false;
                                            return (
                                                <>
                                                    <TableRow
                                                        // className={classes.root}
                                                        hover
                                                        // onClick={(event) => handleClick(event, row.deployment_id)}
                                                        role='checkbox'
                                                        aria-checked={isItemSelected}
                                                        tabIndex={-1}
                                                        key={row.deployment_id}
                                                        selected={isItemSelected}
                                                    >
                                                        <TableCell align='center'>
                                                            <IconButton
                                                                aria-label='expand row'
                                                                size='small'
                                                                onClick={(event) => handleClick(event, row.deployment_id)}
                                                            >
                                                                {isItemSelected ? <ExpandLess /> : <ExpandMore />}
                                                            </IconButton>
                                                        </TableCell>
                                                        <Tooltip
                                                            title={`${row.deployment_id}-deploy23676867676`}
                                                            arrow
                                                            TransitionComponent={Fade}
                                                            TransitionProps={{ timeout: 600 }}
                                                        >
                                                            <TableCell
                                                                align='center'
                                                                component='th'
                                                                id={labelId}
                                                                scope='row'
                                                                className='overflow-text'
                                                            >
                                                                {row.deployment_id}-deploy23676867676
                                                            </TableCell>
                                                        </Tooltip>
                                                        <Tooltip
                                                            title='stateful inspection
                                                            firewall with palo alto base firewall'
                                                            arrow
                                                            TransitionComponent={Fade}
                                                            TransitionProps={{ timeout: 600 }}
                                                        >
                                                            <TableCell align='center' className='overflow-text'>
                                                                stateful inspection firewall with palo alto base firewall
                                                            </TableCell>
                                                        </Tooltip>
                                                        <TableCell
                                                            align='center'
                                                            sx={{
                                                                paddingTop: '2px',
                                                                paddingBottom: '0px',
                                                            }}
                                                        >
                                                            <img
                                                                className='cloud-logo'
                                                                src={row.cloud_provider === 'GCP' ? gcp : row.cloud_provider === 'AWS' ? aws : azure}
                                                                alt='cloud services'
                                                            />
                                                        </TableCell>
                                                        <TableCell align='center'>
                                                            <Chip
                                                                variant='outlined'
                                                                label={row.status}
                                                                color={
                                                                    row.status === 'Running'
                                                                        ? 'success'
                                                                        : row.status === 'Failed'
                                                                        ? 'error'
                                                                        : 'warning'
                                                                }
                                                            />
                                                        </TableCell>
                                                        <TableCell align='center' className={row.status === 'Running' ? undefined : 'ip-hide'}>
                                                            <a
                                                                href={row.public_ip}
                                                                className='ip-link'
                                                                target='_blank'
                                                                rel='noreferrer noopener'
                                                                onMouseEnter={() => setDisplayLaunchIcon(row.deployment_id)}
                                                                onMouseLeave={() => setDisplayLaunchIcon(-1)}
                                                            >
                                                                {row.public_ip === '' ? '-' : row.public_ip}
                                                            </a>
                                                            {displayLaunchIcon === row.deployment_id && (
                                                                <IconButton size='small' onClick={() => window.open(row.public_ip, '_blank')}>
                                                                    <Launch className='ip-link-icon' />
                                                                </IconButton>
                                                            )}
                                                        </TableCell>
                                                        <TableCell align='center'>
                                                            {/* <FormatDate date={row.creation} /> */}
                                                            {row.created_on.split('.')[0].split('T').join(' ')}
                                                        </TableCell>
                                                        <TableCell align='center'>
                                                            <IconButton onClick={() => setOpenLog(true)}>
                                                                <Description className='log-icon' />
                                                            </IconButton>
                                                        </TableCell>
                                                    </TableRow>
                                                    {row.status === 'In Progress' && (
                                                        <TableRow>
                                                            <TableCell
                                                                style={{
                                                                    paddingBottom: 0,
                                                                    paddingTop: 0,
                                                                }}
                                                                colSpan={8}
                                                            >
                                                                <Collapse in={isItemSelected} timeout='auto' unmountOnExit>
                                                                    <Box m={1}>
                                                                        <Grid container>
                                                                            <Grid item xs={1} />
                                                                            <Grid item xs={5}>
                                                                                <Grid item container>
                                                                                    <Grid item xs={3}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Bucket Name</b>
                                                                                        </Typography>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Status</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={9}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            user-123-bucket
                                                                                        </Typography>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            <span className='status-text'>Running</span>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                </Grid>
                                                                            </Grid>
                                                                            <Grid item xs={6}>
                                                                                <Grid item container>
                                                                                    <Grid item xs={3}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Region</b>
                                                                                        </Typography>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Deployment Id</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            US-East-2
                                                                                        </Typography>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            ucz-139
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                </Grid>
                                                                            </Grid>
                                                                        </Grid>
                                                                    </Box>
                                                                </Collapse>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                                    {row.status !== 'In Progress' && (
                                                        <TableRow>
                                                            <TableCell
                                                                style={{
                                                                    paddingBottom: 0,
                                                                    paddingTop: 0,
                                                                }}
                                                                colSpan={8}
                                                            >
                                                                <Collapse in={isItemSelected} timeout='auto' unmountOnExit>
                                                                    <Box m={1}>
                                                                        <Grid container>
                                                                            <Grid item xs={1} />
                                                                            <Grid item xs={5}>
                                                                                <Grid item container>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>IAM Instance Profile</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            IAM-s3-role
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Instance Type</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            m5.xlarge
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>AMI ID</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            ami-0390cf549bf
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Region</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            US East (N. Virginia)
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>VPC Name</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            PA network
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>VPC CIDR</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            172.12.35.50
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Deployment ID</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            {row.deployment_id}-deploy23676867676
                                                                                            <CopyIcon
                                                                                                text={`${row.deployment_id}-deploy23676867676`}
                                                                                                setOpenCopyChip={setOpenCopyChip}
                                                                                                openCopyChip={openCopyChip}
                                                                                            />
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                </Grid>
                                                                            </Grid>
                                                                            <Grid item xs={6}>
                                                                                <Grid item container>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Access Key</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            AKIAIOSFODNN7EX
                                                                                            <CopyIcon
                                                                                                text='AKIAIOSFODNN7EX'
                                                                                                setOpenCopyChip={setOpenCopyChip}
                                                                                                openCopyChip={openCopyChip}
                                                                                            />
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>SSH Key Pair</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            fwdeploy.pem
                                                                                            <CopyIcon
                                                                                                text='fwdeploy.pem'
                                                                                                setOpenCopyChip={setOpenCopyChip}
                                                                                                openCopyChip={openCopyChip}
                                                                                            />
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Product Code</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            6njl1pau431dv1qxipg
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>PAN-OS Version</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            10.0.8-h8
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Management Subnet CIDR</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            172.23.56.79
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Bootstrap Info</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            bootstrapping id
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={4}>
                                                                                        <Typography
                                                                                            sx={{ lineHeight: 2 }}
                                                                                            variant='body2'
                                                                                            className='style-typography'
                                                                                        >
                                                                                            <b>Firewall Name</b>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={0.3}>
                                                                                        <Typography sx={{ lineHeight:2 }} variant='body2'>
                                                                                            :
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6.7}>
                                                                                        <Typography sx={{ lineHeight: 2 }} variant='body2'>
                                                                                            stateful inspection firewall with palo alto base
                                                                                            firewall
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                </Grid>
                                                                            </Grid>
                                                                        </Grid>
                                                                    </Box>
                                                                </Collapse>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                                </>
                                            );
                                        })}
                                    {emptyTable && (
                                        <TableRow
                                            style={{
                                                height: 52,
                                            }}
                                        >
                                            <TableCell colSpan={8} className='no-data-row'>
                                                {noRowFound}
                                            </TableCell>
                                        </TableRow>
                                    )}
                                    {emptyRows > 0 && (
                                        <TableRow
                                            style={{
                                                height: 52 * emptyRows,
                                            }}
                                        >
                                            <TableCell colSpan={8} />
                                        </TableRow>
                                    )}
                                </TableBody>
                            )}
                            {!isLoading && isError && (
                                <TableBody>
                                    <TableRow
                                        style={{
                                            height: 52,
                                        }}
                                    >
                                        <TableCell colSpan={8} className='no-data-row'>
                                            {errorFound}
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            )}
                        </Table>
                    </TableContainer>
                    {isLoading && (
                        <Box>
                            <LinearProgress />
                        </Box>
                    )}
                    {!isLoading && !emptyTable && !isError && (
                        <PaginationFooter
                            page={page}
                            setPage={setPage}
                            rowsPerPage={rowsPerPage}
                            setRowsPerPage={setRowsPerPage}
                            count={Math.ceil(15 / rowsPerPage)}
                        />
                    )}
                </Paper>
            </Box>
            <LogModel openLog={openLog} setOpenLog={setOpenLog} />
        </>
    );
};

export default FirewallList;
