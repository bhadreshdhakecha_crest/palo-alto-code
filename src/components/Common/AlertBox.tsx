import { Alert, Snackbar } from "@mui/material";
import { Dispatch, SetStateAction } from "react";

type Props = {
    open : boolean;
    setOpen : Dispatch<SetStateAction<boolean>>;
    value : string;
};

const AlertBox = (props: Props) => {

    const {open , setOpen, value} = props;

  return (
    <Snackbar
      anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      open={open}
      autoHideDuration={6000}
      onClose={() => setOpen(false)}
    >
      <Alert
        variant="filled"
        onClose={() => setOpen(false)}
        severity="success"
        sx={{ width: "100%" }}
      >
        {value}
      </Alert>
    </Snackbar>
  );
};

export default AlertBox;
