// Firewall List page
export const firewallInitialized: string = "Firewall Deployment Initiated";
export const buttonDeployNew: string = "Deploy New Firewall";
export const rowsPage: string = "Rows Per Page";
export const logModelTitle: string = "Firewall deployment - In Progress";
export const noRowFound = 'No Row Found';
export const errorFound = 'API Credential Invalid'