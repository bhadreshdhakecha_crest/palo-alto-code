import React, { useState } from "react";
import { styled, useTheme, Theme, CSSObject } from "@mui/material/styles";
import {
  Box,
  Badge,
  MenuItem,
  Toolbar,
  List,
  CssBaseline,
  Typography,
  Divider,
  IconButton,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Menu,
  Avatar,
} from "@mui/material";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import {
  AccountCircle,
  Apps,
  Dashboard,
  NotificationsRounded,
  // Person,
  Settings,
} from "@mui/icons-material";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import NotificationsIcon from "@mui/icons-material/Notifications";
import MoreIcon from "@mui/icons-material/MoreVert";
// import AssignmentIcon from "@mui/icons-material/Assignment";
import GppGoodIcon from "@mui/icons-material/GppGood";
import KeyIcon from "@mui/icons-material/Key";
import "./common.scss";
import logo from "./../../assets/images/logo.png";
import smallLogo from "./../../assets/images/smallLogo.png";

//For Avatar
function stringToColor(string: string) {
  let hash = 0;
  let iter: number;

  /* eslint-disable no-bitwise */
  for (iter = 0; iter < string.length; iter += 1) {
    hash = string.charCodeAt(iter) + ((hash << 5) - hash);
  }

  let color: string = "#";

  for (iter = 0; iter < 3; iter += 1) {
    const value = (hash >> (iter * 8)) & 0xff;
    color += `00${value.toString(16)}`.substr(-2);
  }
  /* eslint-enable no-bitwise */

  return color;
}

function stringAvatar(name: string) {
  return {
    sx: {
      bgcolor: stringToColor(name),
    },
    children: `${name.split(" ")[0][0]}${name.split(" ")[1][0]}`,
  };
}

//For AppBar
interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})<AppBarProps>(({ theme, open }) => ({
  ".heading5": {
    fontWeight: "600",
    color: "#4C4C66",
    ...(!open && { marginLeft: theme.spacing(3) }),
  },
  zIndex: theme.zIndex.drawer + 1,
  backgroundColor: "#EEEEEE;",
  color: "black",
  boxShadow: "none",
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

//For Drawer Animation
const drawerWidth: number = 240;
const openedMixin = (theme: Theme): CSSObject => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme: Theme): CSSObject => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

//For Drawer
const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  height: "100vh",
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  color: "red",
  backgroundColor: "#EEEEEE",
  boxShadow: "4px 0px 16px rgba(0, 0, 0, 0.25)",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
  [theme.breakpoints.down("md")]: {
    zIndex: 1,
    position: "fixed",
    overflowX: "hidden",
  },
}));

type Props = {
  children: React.ReactNode;
};

const SideBar = (props: Props) => {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] =
    React.useState<null | HTMLElement>(null);

  const isMenuOpen: boolean = Boolean(anchorEl);

  const isMobileMenuOpen: boolean = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const [selectedMenu, setSelectedMenu] = useState(1);

  const menuList = [
    {
      index: 0,
      value: "Dashboard",
      icon: <Dashboard />,
    },
    {
      index: 1,
      value: "Deploy Firewall",
      icon: <GppGoodIcon />,
    },
    {
      index: 2,
      value: "uCPE Validation",
      icon: <KeyIcon />,
    },
  ];
  const menuId: string = "primary-search-account-menu";

  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
    </Menu>
  );

  const mobileMenuId: string = "primary-search-account-menu-mobile";

  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />

      <AppBar position="fixed" open={open}>
        <Toolbar>
          {!open && (
            <img
              className="smallLogo"
              src={smallLogo}
              onClick={handleDrawerOpen}
              alt="Palo Alto Networks"
            />
          )}
          <Typography className="heading5" variant="h5">
            {menuList[selectedMenu].value}
          </Typography>

          <Box sx={{ flexGrow: 1 }} />

          <Box sx={{ display: { xs: "none", md: "flex" } }}>
            <IconButton
              size="large"
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              // onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <Avatar {...stringAvatar("Harsh Saparia")} />
            </IconButton>
          </Box>
          <Box sx={{ display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}

      <Drawer className="sideBar" variant="permanent" open={open}>
        <DrawerHeader className="sideBar">
          <img className="logo" src={logo} alt="Palo Alto Networks" />
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <List
          sx={{
            fontWeight: theme.typography.fontWeightBold,
          }}
          className="sideBar"
        >
          {menuList.map((menu, index) => {
            return (
              <>
                {index === 3 && <Divider />}
                <ListItemButton
                  key={menu.index}
                  title={menu.value}
                  // onClick={() => setSelectedMenu(menu.index)}
                  sx={{
                    minHeight: 48,
                    justifyContent: open ? "initial" : "center",
                    px: 2.5,
                    backgroundColor: `${
                      selectedMenu === menu.index && theme.palette.primary.main
                    } `,
                    color: `${
                      selectedMenu === menu.index &&
                      theme.palette.primary.contrastText
                    } `,
                    ":hover": {
                      background: `${
                        selectedMenu === menu.index &&
                        theme.palette.primary.main
                      } `,
                    },
                  }}
                >
                  <ListItemIcon
                    // onClick={() => setSelectedMenu(menu.index)}
                    key={"Icon " + index}
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                      color: `${
                        selectedMenu === menu.index &&
                        theme.palette.primary.contrastText
                      } `,
                    }}
                  >
                    {menu.icon}
                  </ListItemIcon>
                  <ListItemText
                    primary={menu.value}
                    sx={{ opacity: open ? 1 : 0 }}
                  />
                </ListItemButton>
              </>
            );
          })}
        </List>
        <Box className="sideBar" sx={{ flexGrow: 1 }} />
      </Drawer>

      <Box
        component="main"
        sx={{
          flexGrow: 1,
          p: 3,
          [theme.breakpoints.down("md")]: {
            paddingLeft: 11,
            opacity: `${open && 0.5}`,
          },
        }}
      >
        <DrawerHeader />
        {props.children}
      </Box>
    </Box>
  );
};

export default SideBar;
