import React from "react";
import IconButton from '@mui/material/IconButton';
import Snackbar from '@mui/material/Snackbar';
import Chip from '@mui/material/Chip';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';

type Props = {text: string;
    setOpenCopyChip: (value: boolean) => void;
    openCopyChip: boolean;}

const CopyIcon = (props: Props) => {
    const { text, setOpenCopyChip, openCopyChip } = props;
    return (
      <>
        <IconButton
          size="small"
          onClick={() => {
            navigator.clipboard.writeText(text);
            setOpenCopyChip(true);
          }}
        >
          <ContentCopyIcon sx={{ fontSize: "15px" }} />
        </IconButton>
        <Snackbar
          open={openCopyChip}
          autoHideDuration={6000}
          onClose={() => setOpenCopyChip(false)}
        >
          <Chip size="small" label="Copied!" />
        </Snackbar>
      </>
    );
}

export default CopyIcon;