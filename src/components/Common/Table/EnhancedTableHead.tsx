import React from 'react';
import { Box, TableCell, TableHead, TableRow, TableSortLabel, Typography } from '@mui/material';
import { visuallyHidden } from '@mui/utils';
import { Data, headCells } from '../../DeployFirewall/FirewallList/data';
import { Order } from './funcTable';

type Props = {
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data) => void;
    order: Order;
    orderBy: string;
};

const EnhancedTableHead = (props: Props) => {
    const { order, orderBy, onRequestSort } = props;
    const createSortHandler = (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead className='table-head'>
            <TableRow>
                <TableCell width='4%' />
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.postionCenter ? 'center' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.id ? order : false}
                        width={headCell.width}
                    >
                        {orderBy === headCell.id ? (
                            <TableSortLabel
                                active={orderBy === headCell.id}
                                direction={orderBy === headCell.id ? order : 'asc'}
                                onClick={createSortHandler(headCell.id)}
                                sx={{ marginLeft: '25px' }}
                            >
                                <Typography variant='body1' className='table-head-typography'>
                                    <b>{headCell.label}</b>
                                </Typography>
                                <Box component='span' sx={visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </Box>
                            </TableSortLabel>
                        ) : (
                            <Typography variant='body1' className='table-head-typography'>
                                <b>{headCell.label}</b>
                            </Typography>
                        )}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
};

export default EnhancedTableHead;
