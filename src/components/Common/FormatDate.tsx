import React from "react";

type Props = { date: Date };

const FormatDate = (props: Props) => {
  const { date } = props;

  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  const day = date.getDate();
  const dayDelimiter =
    day === 1 ? "st" : day === 2 ? "nd" : day === 3 ? "rd" : "th";

  const monthIndex = date.getMonth();
  const monthName = monthNames[monthIndex];

  const year = date.getFullYear();

  const time = date.toLocaleString("en-US", {
    hour: "numeric",
    minute: "numeric",
    hour12: true,
  });

  return (
    <>
      {day}
      <sup>{dayDelimiter}</sup> {monthName} {year} &nbsp;
      {/* <br/> */}
      {time}
    </>
  );
};

export default FormatDate;
