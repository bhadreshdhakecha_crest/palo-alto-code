import {
  Button,
  Grid,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import './common.scss';

type Props = {
  children: React.ReactNode;
};

const LoginRegister = (props: Props) => {
  const theme = useTheme();
  const matchSM = useMediaQuery(theme.breakpoints.down("sm"));
  return (
    <Grid container className="gridMain">
      <Grid
        item
        container
        direction="column"
        justifyContent="center"
        md={7}
        className="gridLeft"
      >
        <Grid item className="leftContent">
          <Typography variant={matchSM ? "h4" : "h3"}>
            <b>Palo Alto: Automation</b>
          </Typography>
          <Typography variant="body1">
            The global cybersecurity leader
          </Typography>
          <Button variant="contained" className="readButton">
            Read More
          </Button>
        </Grid>
      </Grid>
      <Grid
        item
        container
        direction="column"
        justifyContent="center"
        md={5}
        spacing={2}
        className="gridRight"
        alignItems="center"
      >
        {props.children}
      </Grid>
    </Grid>
  );
};

export default LoginRegister;
