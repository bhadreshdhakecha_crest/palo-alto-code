import { Link } from "react-router-dom";
import {
  Email as EmailIcon,
  Home as OrganizationIcon,
  Lock as PasswordIcon,
  Person as NameIcon,
} from "@mui/icons-material";
import {
  Button,
  Grid,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import LoginRegister from "../Common/LoginRegister";
import "./register.scss";

type Props = {};

const Login = (props: Props) => {
  return (
    <LoginRegister>
      <Grid item>
        <Typography variant="h4" mb={2}>
          <b> Sign Up </b>
        </Typography>
      </Grid>
      <Grid item>
        <TextField
          required
          variant="outlined"
          className="loginFormField"
          placeholder="Full Name"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <NameIcon />
              </InputAdornment>
            ),
          }}
        />
      </Grid>
      <Grid item>
        <TextField
          required
          variant="outlined"
          className="loginFormField"
          placeholder="Email Address"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <EmailIcon />
              </InputAdornment>
            ),
          }}
        />
      </Grid>
      <Grid item>
        <TextField
          required
          variant="outlined"
          className="loginFormField"
          placeholder="Organization"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <OrganizationIcon />
              </InputAdornment>
            ),
          }}
        />
      </Grid>
      <Grid item>
        <TextField
          required
          variant="outlined"
          className="loginFormField"
          placeholder="Password"
          type="password"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <PasswordIcon />
              </InputAdornment>
            ),
          }}
        />
      </Grid>
      <Grid item>
        <TextField
          required
          variant="outlined"
          className="loginFormField"
          placeholder="Confirm Password"
          type="password"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <PasswordIcon />
              </InputAdornment>
            ),
          }}
        />
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          className="registerButton"
          component={Link}
          to="../deploy-firewall-dashboard"
        >
          {" "}
          Register{" "}
        </Button>
      </Grid>
      <Grid item>
        <Typography variant="body2" display="inline">
          Do you already have account?{" "}
        </Typography>
        <Typography
          variant="body2"
          component={Link}
          to="../login"
          className="registerLink"
          display="inline"
        >
          Sign In
        </Typography>
      </Grid>
    </LoginRegister>
  );
};

export default Login;
