import React from "react";
import { alpha } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import Checkbox from "@mui/material/Checkbox";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import DeleteIcon from "@mui/icons-material/Delete";
import { visuallyHidden } from "@mui/utils";
import SideBar from "../Common/SideBar";
import {
  ExpandMore,
  MoreVert,
  Search,
  UndoOutlined,
} from "@mui/icons-material";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Chip,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  OutlinedInput,
} from "@mui/material";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { withStyles } from "@mui/styles";
import { emailArr, orgArr, usernameArr, rows, Data } from "./data";

type Props = {};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Order = "asc" | "desc";

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key
): (
  a: { [key in Key]: number | string },
  b: { [key in Key]: number | string }
) => number {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
function stableSort<T>(
  array: readonly T[],
  comparator: (a: T, b: T) => number
) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
  numeric: boolean;
}

const headCells: readonly HeadCell[] = [
  {
    id: "username",
    numeric: false,
    disablePadding: false,
    label: "Username",
  },
  {
    id: "email",
    numeric: false,
    disablePadding: false,
    label: "Email",
  },
  {
    id: "organization",
    numeric: false,
    disablePadding: false,
    label: "Organization",
  },
  {
    id: "message",
    numeric: false,
    disablePadding: false,
    label: "Message",
  },
  {
    id: "create",
    numeric: false,
    disablePadding: false,
    label: "Created On",
  },
];

interface EnhancedTableProps {
  numSelected: number;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const {
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort,
  } = props;
  const createSortHandler =
    (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead className="table-head">
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              "aria-label": "select all users",
            }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              <Typography variant="body1" className="table-head-typography">
                <b>{headCell.label}</b>
              </Typography>
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface EnhancedTableToolbarProps {
  numSelected: number;
}

const StyledAccordionSummary = withStyles({
  root: {
    minHeight: 40,
    maxHeight: 40,
    "&.Mui-expanded": {
      minHeight: 40,
      maxHeight: 40,
    },
  },
  expandIcon: {
    order: -1,
  },
})(AccordionSummary);

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const { numSelected } = props;

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        ...(numSelected > 0 && {
          bgcolor: (theme) =>
            alpha(
              theme.palette.primary.main,
              theme.palette.action.activatedOpacity
            ),
        }),
      }}
    >
      {numSelected > 0 ? (
        <Typography
          sx={{ flex: "1 1 100%" }}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          {numSelected} selected
        </Typography>
      ) : (
        <Typography
          sx={{ flex: "1 1 100%" }}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          Show Activities
        </Typography>
      )}
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : null}
    </Toolbar>
  );
};

const ShowActivities = (props: Props) => {
  const [order, setOrder] = React.useState<Order>("asc");
  const [orderBy, setOrderBy] = React.useState<keyof Data>("username");
  const [selected, setSelected] = React.useState<readonly string[]>([]);
  const [page, setPage] = React.useState(0);
  const dense = true;
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [expanded, setExpanded] = React.useState<string | false>("searchPanel");
  const [searchUsername, setSearchUsername] = React.useState<string[]>([]);
  const [searchEmail, setSearchEmail] = React.useState<string[]>([]);
  const [searchOrganization, setSearchOrganization] = React.useState<string[]>(
    []
  );
  const [search, setSearch] = React.useState<string[]>([]);
  const [rowData, setRowData] = React.useState<Data[]>(rows);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = rowData.map((n) => n.username);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeSearchAccordion =
    (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };

  const handleSearchUsername = (
    event: SelectChangeEvent<typeof searchUsername>
  ) => {
    const {
      target: { value },
    } = event;
    setSearchUsername(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  const handleSearchEmail = (event: SelectChangeEvent<typeof searchEmail>) => {
    const {
      target: { value },
    } = event;
    setSearchEmail(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  const handleSearchOrganization = (
    event: SelectChangeEvent<typeof searchOrganization>
  ) => {
    const {
      target: { value },
    } = event;
    setSearchOrganization(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  const handleSearchReset = () => {
    setSearchUsername([]);
    setSearchEmail([]);
    setSearchOrganization([]);
    setSearch([]);
    setRowData(rows);
    setExpanded(false);
  };

  const handleSearch = async () => {
    let searchStringArr: string[] = [];
    let searchIndex = 0;
    if (searchUsername.length) {
      searchStringArr[searchIndex] = "Username : " + searchUsername.join(" | ");
      ++searchIndex;
    }
    if (searchEmail.length) {
      searchStringArr[searchIndex] = "Email : " + searchEmail.join(" | ");
      ++searchIndex;
    }
    if (searchOrganization.length) {
      searchStringArr[searchIndex] =
        "Organization : " + searchOrganization.join(" | ");
      ++searchIndex;
    }
    if (searchIndex) {
      let tableData: Data[] = [];
      for await (let row of rows) {
        if (
          (searchUsername.length && searchUsername.includes(row.username)) ||
          (searchEmail.length && searchEmail.includes(row.email)) ||
          (searchOrganization.length &&
            searchOrganization.includes(row.organization))
        ) {
          tableData.push(row);
        }
      }
      setRowData(tableData);
    } else {
      setRowData(rows);
    }
    setSearch(searchStringArr);
    setExpanded(false);
  };

  const isSelected = (name: string) => selected.indexOf(name) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rowData.length) : 0;

  return (
    <SideBar>
      <Grid container spacing={1} mb={2}>
        <Grid item sx={{ flexGrow: 1 }}>
          <Paper>
            <Accordion
              expanded={expanded === "searchPanel"}
              onChange={handleChangeSearchAccordion("searchPanel")}
            >
              <StyledAccordionSummary
                expandIcon={<ExpandMore />}
                aria-controls="panel1bh-content"
                id="panel1bh-header"
              >
                <Typography sx={{ width: "5%", flexShrink: 0 }}>
                  Filters
                </Typography>
                <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                  {search.map((value, index) => (
                    <Chip
                      key={index}
                      size="small"
                      label={value}
                      onDelete={() => {
                        const columnKey = value.split(" ")[0];
                        switch (columnKey) {
                          case "Username":
                            searchUsername.length = 0;
                            break;
                          case "Email":
                            searchEmail.length = 0;
                            break;
                          case "Organization":
                            searchOrganization.length = 0;
                            break;

                          default:
                            break;
                        }
                        handleSearch();
                      }}
                    />
                  ))}
                </Box>
              </StyledAccordionSummary>
              <AccordionDetails>
                <Grid container spacing={2} mb={2}>
                  <Grid item xs>
                    <FormControl sx={{ width: "100%" }}>
                      <InputLabel shrink id="username-label">
                        Username
                      </InputLabel>
                      <Select
                        labelId="username-label"
                        id="username"
                        size="small"
                        fullWidth
                        multiple
                        value={searchUsername}
                        onChange={handleSearchUsername}
                        input={<OutlinedInput notched label="Username" />}
                        renderValue={(selected) => (
                          <Box
                            sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}
                          >
                            {selected.map((value) => (
                              <Chip key={value} size="small" label={value} />
                            ))}
                          </Box>
                        )}
                        MenuProps={MenuProps}
                      >
                        {usernameArr.map((value, index) => (
                          <MenuItem key={index} value={value}>
                            {value}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs>
                    <FormControl sx={{ width: "100%" }}>
                      <InputLabel shrink id="email-label">
                        Email
                      </InputLabel>
                      <Select
                        labelId="email-label"
                        id="email"
                        size="small"
                        fullWidth
                        multiple
                        value={searchEmail}
                        onChange={handleSearchEmail}
                        input={<OutlinedInput notched label="Email" />}
                        renderValue={(selected) => (
                          <Box
                            sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}
                          >
                            {selected.map((value) => (
                              <Chip key={value} size="small" label={value} />
                            ))}
                          </Box>
                        )}
                        MenuProps={MenuProps}
                      >
                        {emailArr.map((value, index) => (
                          <MenuItem key={index} value={value}>
                            {value}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs>
                    <FormControl sx={{ width: "100%" }}>
                      <InputLabel shrink id="organization-label">
                        Organization
                      </InputLabel>
                      <Select
                        labelId="organization-label"
                        id="organization"
                        size="small"
                        fullWidth
                        multiple
                        value={searchOrganization}
                        onChange={handleSearchOrganization}
                        input={<OutlinedInput notched label="Organization" />}
                        renderValue={(selected) => (
                          <Box
                            sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}
                          >
                            {selected.map((value) => (
                              <Chip key={value} size="small" label={value} />
                            ))}
                          </Box>
                        )}
                        MenuProps={MenuProps}
                      >
                        {orgArr.map((value, index) => (
                          <MenuItem key={index} value={value}>
                            {value}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
                <Grid container justifyContent="flex-end" spacing={2}>
                  <Grid item>
                    <Button
                      variant="contained"
                      size="small"
                      onClick={handleSearch}
                      startIcon={<Search />}
                    >
                      Search
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      variant="text"
                      size="small"
                      onClick={handleSearchReset}
                      startIcon={<UndoOutlined />}
                    >
                      Reset
                    </Button>
                  </Grid>
                </Grid>
              </AccordionDetails>
            </Accordion>
          </Paper>
        </Grid>
        <Grid item>
          <Paper>
            <IconButton aria-label="more">
              <MoreVert fontSize="inherit" />
            </IconButton>
          </Paper>
        </Grid>
      </Grid>
      <Paper sx={{ width: "100%", mb: 2 }} elevation={2}>
        <EnhancedTableToolbar numSelected={selected.length} />
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size={dense ? "small" : "medium"}
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rowData.length}
            />
            <TableBody>
              {/* if you don't need to support IE11, you can replace the `stableSort` call with:
              rows.slice().sort(getComparator(order, orderBy)) */}
              {stableSort(rowData, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.username);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row.username)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.username}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          color="primary"
                          checked={isItemSelected}
                          inputProps={{
                            "aria-labelledby": labelId,
                          }}
                        />
                      </TableCell>
                      <TableCell component="th" id={labelId} scope="row">
                        {row.username}
                      </TableCell>
                      <TableCell>{row.email}</TableCell>
                      <TableCell>{row.organization}</TableCell>
                      <TableCell>{row.message}</TableCell>
                      <TableCell>{row.create}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: (dense ? 33 : 53) * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rowData.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </SideBar>
  );
};

export default ShowActivities;
