export const columns: string[] = [
  "username",
  "email",
  "organization",
  "message",
  "created",
];

export interface Data {
  username: string;
  email: string;
  organization: string;
  message: string;
  create: string;
}

function createData(
  username: string,
  email: string,
  organization: string,
  message: string,
  create: string
): Data {
  return {
    username,
    email,
    organization,
    message,
    create,
  };
}

export const rows = [
  createData(
    "User1",
    "user1@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User2",
    "user2@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User3",
    "user3@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User4",
    "user4@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User5",
    "user5@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User6",
    "user6@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User7",
    "user7@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User8",
    "user8@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User9",
    "user9@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User10",
    "user10@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User11",
    "user11@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User12",
    "user12@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User13",
    "user13@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User14",
    "user14@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User15",
    "user15@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User16",
    "user16@gmail.com",
    "org-1",
    "License key Expired",
    "3 March 2022 5:00 PM"
  ),
  createData(
    "User17",
    "user17@gmail.com",
    "org-1",
    "New key Generated",
    "3 March 2022 5:00 PM"
  ),
];

export const usernameArr = ["User1", "User2", "User3", "User4", "User5", "User6", "User7", "User8", "User9", "user10"];
export const emailArr = ["user1@gmail.com", "user2@gmail.com", "user3@gmail.com", "user4@gmail.com", "user5@gmail.com", "user6@gmail.com", "user7@gmail.com", "user8@gmail.com", "user9@gmail.com", "user10@gmail.com", "user11@gmail.com"];
export const orgArr = ["org-1"];
export const MessageArr = ["New key Generated", "License key Expired"];