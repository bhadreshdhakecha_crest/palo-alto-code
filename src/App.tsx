import { Navigate, Route, Routes } from "react-router";
import Login from "./components/Login/Login";
import Register from "./components/Register/Register";
import ShowActivities from "./components/ShowActivities/ShowActivities";
import Deploy from "./components/DeployFirewall/Deploy/Deploy";
import { Provider } from "react-redux";
import store from "./store/store";
import { createTheme, ThemeProvider } from "@mui/material/styles";
function App() {
  let theme = createTheme({
    palette: {
      primary: {
        main: "#EE582D",
        contrastText: "#FFFFFF",
      },
    },
    typography: {
      fontFamily: "Decimal,'Helvetica Neue',Helvetica,sans-serif",
      fontWeightMedium: 500,
    },
  });
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Routes>
          <Route path="/" element={<Navigate to="/login" />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/deploy-firewall" element={<Deploy />} />
          <Route path="/show-activities" element={<ShowActivities />} />
        </Routes>
      </Provider>
    </ThemeProvider>
  );
}

export default App;
