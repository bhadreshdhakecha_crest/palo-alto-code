import { createSlice, configureStore } from "@reduxjs/toolkit";

const deployFirewall = {
  formData: {
    activeStep: -1,
    noFirewall:false,
    cloudProvider: "",
    region: "",
    amiId: "",
    instanceSize: "",
    secretKey: "",
    accessKey: "",
    bucketName: "",
    vpcNetworkName: "",
    vpcIP: "",
    webSubnetIP: "",
    publicSubnetIP: "",
    serverKey: "",
    stackName: "",
  },
};

const myDeployFirewall = createSlice({
  initialState: deployFirewall,
  name: "deployFirewall",
  reducers: {
    updateFormData(state, action) {
      switch (action.payload.name) {
        case "backStep":
          state.formData.activeStep -= 1;
          break;
        case "nextStep":
          state.formData.activeStep += 1;
          break;
        case "noFirewall":
          state.formData.noFirewall=!state.formData.noFirewall;
          break;
        case "gotToStepOne":
          state.formData.activeStep = 0;
          break;
        case "gotToStepTwo":
          state.formData.activeStep = 2;
          break;
        case "gotToStepThree":
          state.formData.activeStep = 3;
          break;
        case "cloudProvider":
          state.formData.cloudProvider = action.payload.value;
          state.formData.activeStep = 1;
          break;
        case "region":
          state.formData.region = action.payload.value;
          state.formData.amiId = action.payload.amiId;
          break;
        case "instanceSize":
          state.formData.instanceSize = action.payload.value;
          break;
        case "secretKey":
          state.formData.secretKey = action.payload.value;
          break;
        case "accessKey":
          state.formData.accessKey = action.payload.value;
          break;
        case "AmiId":
          state.formData.amiId = action.payload.value;
          break;
        case "bucketName":
          state.formData.bucketName = action.payload.value;
          break;
        case "vpcName":
          state.formData.vpcNetworkName = action.payload.value;
          break;
        case "vpcIP":
          state.formData.vpcIP = action.payload.value;
          break;
        case "webSubIP":
          state.formData.webSubnetIP = action.payload.value;
          break;
        case "pubSubIP":
          state.formData.publicSubnetIP = action.payload.value;
          break;
        case "serverKey":
          state.formData.serverKey = action.payload.value;
          break;
        case "stackName":
          state.formData.stackName = action.payload.value;
          break;
        default:
          state.formData = { ...state.formData };
      }
    },
  },
});

const store = configureStore({
  reducer: {
    deployFirewallReducer: myDeployFirewall.reducer,
  },
});

export const deployFirewallActions = myDeployFirewall.actions;

export default store;
